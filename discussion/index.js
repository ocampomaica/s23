/*
	Objects
	an object is a data type that is used to reperesent reald world objects. It is also a collection of related data and/or functionalities.

	Creating objects using object literal:
	Syntax:
		let objectName = {
			keyA: valueA
			keyB: valueB
		}
*/

let student = {
	firstName: "Rupert",
	lastName: "Ramos",
	Age: 30,
	StudentID: "2022-009752",
	email: ["rupert.ramos@gmail.com"],
	address: {
		street: "125 Ilang-ilang St.",
		city: "Quezon City",
		country: "Philippines"
	}

}

console.log("Result from Creating  an Objects:")
console.log(student)
console.log(typeof student)

//Creating Objects using Constructor Function
/*
	Creates a reusable function to create several objects that have the same data structure.This is useful for creating multiple instances/copies of an object.

	Syntax:
	function objectName(valueA, valueB){
		this.keyA = valueA
		this.keyB = valueB

		let variable = new function objectName(valueA, valueB)

		console.log(variable)

			-"this" is a keyword that is used for invoking; in refers to the global object 
			- don't forget to add "new" keyword when creating the variables
	}
*/

function Laptop(name, manufactureDate){
	this.name = name 
	this.manufactureDate = manufactureDate
}

let laptop = new Laptop("Lenovo", 2008)
console.log("Result of creating object")
console.log(laptop)

let myLaptop = new Laptop("Mac Book Air", [2020, 2021])
console.log("Result of creating object")
console.log(myLaptop)

let oldLaptop = Laptop("Portal R2E CCMC", 1980)
console.log("Result of creating object")
console.log(oldLaptop)

//Creating empty object as placeholder
let computer = {}
let myComputer = new Object()
console.log(computer)
console.log(myComputer)

myComputer = {
	name: "Asus",
	manufactureDate: 2012
}

console.log(myComputer)
console.log("")
/*
MINI ACTIVITY
	Create an object constructor function to produce 2 objects with 3 key-value pairs
	Log the 2 new objects in the console and send SS in our GC
*/

//Accessing Object Property
/*
	using the dot notation
	Syntax: objectName.propertyName
*/

console.log("Result from dot notation: DDDDD " + myLaptop.name)

/*
	using the bracket notation
	Syntax: objectName["name"]
*/

console.log("Result from dot notation: " + myLaptop["name"])

//Accessing Array Object

let array = [laptop, myLaptop]
//let array[{name: "Lenovo", manufactureDate: 2008}, {name: "Macbook Air", manufactureDate: [2019,2020]}]

//Dot Notation
console.log(array[0].name)

//Square Bracket Notation
console.log(array[0]["name"])

//Initializind, Adding, Deleting, Reassigning Object Properties

let car = {}
console.log(car)

//Adding Object Properties
car.name = "Honda Civic"
console.log("Result from adding property using dot notation:" )
console.log(car)

car.name = ["Ferrari", "Toyota"]
console.log(car)

car["manufactureDate"] = 2019
console.log(car)

//Deleting Object Properties
delete car["manufactureDate"]
//car["manufactureDate"] = " "
console.log("Result from deleting property using bracket notation:" )
console.log(car)

//Reassigning Object Properties
car.name = "Tesla"
console.log("Result from reassigning property using dot notation:" )
console.log(car)

console.log("")

/*
	Object Method
	a method where a function serves as a value in a property. They are also functions and one of the key differences they have iis that methods are function related to a specific object property.

*/

let person = {
	name: "John",
	talk: function(){
		console.log("Hello! My name is " + this.name)
	}
}

console.log(person)
console.log("Result from object methods:")
person.talk()

person.walk = function() {
	console.log(this.name + " walked 25 steps forward.")
}

person.walk()

let friend  = {
	firstName: "John",
	lastName: "Doe",
	address: {
		city: "Austin, Texas",
		country: "US"
	},
	emails: ["johnD@mail.com", "joel12@yahoo.com"],
	introduce: function(){
		console.log("Hello! My name is " + this.firstName + " " + this.lastName)
	}
}

friend.introduce()

// Real World Application
/*
	Scenario:
		1. We would like to create a game that would have several pokemon to interact with each other.
		1. Every pokemon would have the same sets of stats, properties, and functions.
*/

//Using Object Literals
let myPokemon = {
	name: "Pikachu",
	level: 3,
	health: 100,
	attack: 50,
	tackle: function(){
		console.log("This pokemon tackled target Pokemon")
		console.log("target Pokemon's healt is now reduced to targetPokemonHealth")
	},
	faint: function(){
		console.log("Pokemon fainted")
	}
}

console.log(myPokemon)

//Using Object Constructor
function Pokemon(name, level){
	this.name = name
	this.level = level
	this.health = 3 * level
	this.attack = 2 * level

	//Methods
	this.tackle = function(target){
		console.log(this.name + " tackled " + target.name)
		console.log(target.name + "'s health iw now reduced to " + (target.health - this.attack))
	}
	this.faint = function() {
		console.log(this.name + "fainted.")
	}
}

let charizard = new Pokemon("Charizard", 12)
let squirtle = new Pokemon("Squirtle", 6)

console.log(charizard)
console.log(squirtle)

charizard.tackle(squirtle)



