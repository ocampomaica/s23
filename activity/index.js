
//MAIN ACTIVITY:
/*
WDC028v1.5b-23 | JavaScript - Objects
Graded Activity:
Part 1: 
    /1. Initialize/add the following trainer object properties:
      Name (String)
      Age (Number)
      Pokemon (Array)
      Friends (Object with Array values for properties)
    /2. Initialize/add the trainer object method named talk that prints out the message Pikachu! I choose you!
    3. Access the trainer object properties using dot and square bracket notation.
    /4. Invoke/call the trainer talk object method.
*/
//Code Here:

let trainer = {
  name: "Ash Ketchum",
  age: 10,
  pokemon: "Pikachu",
  friends: ["Misty", "Brock", "Tracey"],
  talk: function(){
    console.log(this.pokemon + "! I choose you!")
  }
}

console.log(trainer.name + "'s Pokemon is " +trainer["pokemon"])

console.log(trainer)
trainer.talk()

/*Part 2:

1.) Create a new set of pokemon for pokemon battle. (Same as our discussion)
  - Solve the health of the pokemon that when tackle is invoked, current value of target's health should decrease continuously as many times the tackle is invoked.
  (target.health - this.attack)

2.) If health is less than or equal to 5, invoke faint function*/
//Code Here:
function pokemon(name, level){
  this.name = name
  this.level = level
  this.health = 5 * level
  this.attack = 2 * level

  this.faint = function(target){
    console.log(this.name + " fainted.")
  }

  this.tackle = function(target){
    console.log(this.name + " tackled " + target.name)
    let totalDmg = target.health - (target.health - this.attack)
    target.health = target.health - this.attack;

    if(target.health <= 5){
      console.log(target.name + " received " + totalDmg + " total damage. ")
      target.faint(target.name)
      
    }else {
      console.log(target.name + " received " + totalDmg+ " total damage. ")
      console.log(target.name + "'s HP: " + target.health)

    }
  }

  //this.faint = faint
}

let snorlax = new pokemon("Snorlax", 4)
let sylveon = new pokemon("Sylveon", 5)

console.log(snorlax)
console.log(sylveon)

console.log("")
snorlax.tackle(sylveon)

console.log("")
sylveon.tackle(snorlax)

console.log("")
snorlax.tackle(sylveon)

console.log("")
sylveon.tackle(snorlax)